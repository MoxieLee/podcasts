# Podcast Liste

## Allgemein
* https://wrint.de/

## Gechichte
* https://www.zeitsprung.fm/
* https://www.deutschlandfunknova.de/eine-stunde-history

## IT und Technik
* https://freakshow.de
* https://logbuch-netzpolitik.de/
* https://forschergeist.de/
* https://dtr.fm
* https://workingdraft.fm
* https://omegataupodcast.net/
* https://resonator-podcast.de/

## Finanzen
* https://www.gaborsteingart.com/der-podcast/
* https://finanzrocker.net/tag/podcast/
* https://www.finanzwesir.com/
* Boerse Radio Network AG: https://www.brn-ag.de/
* https://immopreneur.de/podcast/

## Politik Gesellschaft
* https://www.falter.at/falter/radio



